//сортування бульбашкою
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define SIZE 10

void swap(int *arr1, int *arr2)
{
    int buf;
    buf = *arr1;
    *arr1 = *arr2;
    *arr2 = buf;
}

int main()
{
    int arr[SIZE];
    srand(time(0));
    bool sort = true;
    int max = 10;
    int min = -10;

    printf("\n");

    for (int i = 0; i < SIZE; i++)
    {
        arr[i] = rand() % (max - min + 1) + min;
        printf("%i  ", arr[i]);
    }
    printf("\n");

    for (int j = 0; j < SIZE && sort; j++)
    {
        sort = false;
        for (int i = 0; i < SIZE - 1; i++)
        {
            if (arr[i] > arr[i + 1])
            {
                swap(&arr[i], &arr[i + 1]);
                sort = true;
            }
        }
    }

    for (int i = 0; i < SIZE; i++)
    {
        printf("%i  ", arr[i]);
    }
    printf("\n\n");

    return 0;
}