#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 5

int main()
{
    int arr[SIZE];
    int mask[SIZE];
    int max = 5;
    int min = -5;
    int one = 1;
    int zer = 0;
    int mult = 1;
    srand(time(0));

    for (int i = 0; i < SIZE; i++)
    {
        arr[i] = rand() % (max - min + 1) + min;
        printf("%i  ", arr[i]);
    }
    printf("\n");

    for (int i = 0; i < SIZE; i++)
    {
        mask[i] = rand() % (one - zer + 1) + zer;
        printf("%i  ", mask[i]);
    }
    printf("\n");

    for(int i = 0; i < SIZE; i++)
    {
        if(mask[i] == 1)
        {
            mult *= arr[i];
        }
    }

    printf("\n%i\n", mult);

    return 0;
}