#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

int main()
{
    char arr[] = "a, b, c, abc";
    int num = 0;
    bool isWord = false;

    for(int i = 0; arr[i] != '\0'; i++)
    {
        if(isalpha(arr[i]))
        {
            isWord = true;
            continue;
        }

        if(!isalpha(arr[i]) && isWord == true)
        {
            isWord = false;
            num++;
        }
    }
    if(isWord)
    {
        num++;
    }
    printf("Num: %i\n", num);

    return 0;
}