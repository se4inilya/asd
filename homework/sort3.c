//шейкерне сортування
#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 10

void swap(int *arr1, int *arr2)
{
    int buf;
    buf = *arr1;
    *arr1 = *arr2;
    *arr2 = buf;
}

int main()
{
    srand(time(0));
    int arr[SIZE];
    int leftB = 0;
    int rightB = SIZE - 1;
    int max = 100;
    int min = -100;

    printf("\n");

    for (int i = 0; i < SIZE; i++)
    {
        arr[i] = rand() % (max - min + 1) + min;
        printf("%i  ", arr[i]);
    }
    printf("\n");

    for (int i = 0;; i++)
    {
        if (leftB > rightB)
        {
            break;
        }

        for (int j = leftB; j < rightB; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                swap(&arr[j], &arr[j + 1]);
            }
        }
        rightB--;

        for (int j = rightB; j > leftB; j--)
        {
            if (arr[j] < arr[j - 1])
            {
                swap(&arr[j], &arr[j - 1]);
            }
        }
        leftB++;
    }

    for (int i = 0; i < SIZE; i++)
    {
        printf("%i  ", arr[i]);
    }
    printf("\n\n");

    return 0;
}