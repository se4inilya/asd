#include <stdio.h>
#include <stdlib.h>
#include <progbase.h>
#include <assert.h>
#include <time.h>
#include <stdbool.h>

void swap(int *x,int *y);
int choose_pivot(int i,int j );
void quicksort(int list[],int m,int n);
void fillingMatrix(int N, int Arr[N][N]);
void printMatrix(int N, int Arr[N][N]);
void fillingArray(int N, int Arr[N][N], int list[]);
void fillingSortedMatrix(int N, int Arr[N][N], int list[]);
void printList(int size, int list[]);
void test();

int main()
{
    srand(time(0));
    test();

    int N = 0;
    printf("Enter N :");
    scanf("%i", &N);
    int Arr[N][N];
    int size = (N * N - 2 * N + N % 2) / 4;
    int list[size];

    fillingMatrix(N, Arr);
    printMatrix(N, Arr);
    fillingArray(N, Arr, list);
    
    
    printList(size, list);
    puts("");

    quicksort(list, 0, size - 1);

    printList(size, list);
    puts("");
    
    fillingSortedMatrix(N, Arr, list);

    printMatrix(N, Arr);

    return 0;
}

void swap(int *x,int *y)
{
    int temp;
    temp = *x;
    *x = *y;
    *y = temp;
}
 
int choose_pivot(int i,int j )
{
    return((i+j) /2);
}

void quicksort(int list[],int m,int n)
{
    int key,i,j,k;
    if( m < n)
    {
        k = choose_pivot(m,n);
        swap(&list[m],&list[k]);
        key = list[m];
        i = m+1;
        j = n;
        while(i <= j)
        {
            while((i <= n) && (list[i] <= key))
                i++;
            while((j >= m) && (list[j] > key))
                j--;
            if( i < j)
                swap(&list[i],&list[j]);
        }
        /* swap two elements */
        swap(&list[m],&list[j]);
 
        /* recursively sort the lesser list */
        quicksort(list,m,j-1);
        quicksort(list,j+1,n);
    }
}

void fillingMatrix(int N, int Arr[N][N]){
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            Arr[i][j] = rand() % (100 - 1 + 1) + 1;
        }
    }
}
void printMatrix(int N, int Arr[N][N]){
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            printf("%i    ", Arr[i][j]);
        }
        puts("");
    }
    puts("");
}
void fillingArray(int N, int Arr[N][N], int list[]){
    int x = 0;
    int counter = 0;
    if(N % 2 == 0){
        x = N / 2 - 2;
    }
    else{
        x = N / 2 - 1;
    }
    int y = N / 2;
    int d = 1;
    while(!((y == 0) || y == N - 1)){
        list[counter] = Arr[y][x];
        counter++;
        y -= d;
        if(x == y){
            x -= 1;
            d = -d;
        }
        if(x + y == N - 1){
            x--;
            d = -d;        
        }
    }
}
void printList(int size, int list[]){
    for(int i = 0; i < size; i++){
        printf("%i ", list[i]);
    }
    puts("");
}
void fillingSortedMatrix(int N, int Arr[N][N], int list[]){
    int x = 0;
    int counter = 0;
    if(N % 2 == 0){
        x = N / 2 - 2;
    }
    else{
        x = N / 2 - 1;
    }
    int y = N / 2;
    int d = 1;
    while(!((y == 0) || y == N - 1)){
        Arr[y][x] = list[counter];
        counter++;
        y -= d;
        if(x == y){
            x -= 1;
            d = -d;
        }
        if(x + y == N - 1){
            x--;
            d = -d;        
        }
    }
}

void test(){
    int x = 8;
    int y = 5;

    swap(&x, &y);
    assert(x == 5 && y == 8);

    int z = choose_pivot(x, y);
    assert(z == 6);

    int list[3] = {7, -5, 20};
    quicksort(list, 0, 2);
    assert(list[0] == -5 && list[1] == 7 && list[2] == 20);

    int N = 4;
    //int size = (N * N - 2 * N + N % 2) / 4;
    int mat[4][4] = {{5, 7, 9, 11}, {4, 50, 101, 5}, {-1, 5, 6, 8}, {9, 8, 5, 10}};
    fillingArray(N, mat, list);
    assert(list[0] == -1 && list[1] == 4);

    list[0] = 6;
    list[1] = 0;
    fillingSortedMatrix(N, mat, list);
    assert(mat[1][0] == 0 && mat[2][0] == 6);
}