//insert sort сортування вставками
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define SIZE 10

void swap(int *arr1, int *arr2)
{
    int buf;
    buf = *arr1;
    *arr1 = *arr2;
    *arr2 = buf;
}

int main()
{
    int arr[SIZE];
    int max = 10;
    int min = -10;
    srand(time(0));

    for (int i = 0; i < SIZE; i++)
    {
        arr[i] = rand() % (max - min + 1) + min;
        printf("%i  ", arr[i]);
    }
    printf("\n");

    for (int i = 0; i < SIZE; i++)
    {
        int j = i;
        while (arr[j] < arr[j - 1])
        {
            swap(&arr[j], &arr[j - 1]);
            j--;
        }
    }

    for (int i = 0; i < SIZE; i++)
    {
        printf("%i  ", arr[i]);
    }
    printf("\n\n");

    return 0;
}