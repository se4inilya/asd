#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <assert.h>

struct SLNode
{
    int data;
    struct SLNode *next;
};
struct DLNode
{
    int data;
    struct DLNode *prev;
    struct DLNode *next;
};

void test();
struct SLNode *createSLNode(int data);
struct DLNode *createDLNode(int data);
struct DLNode *addDLNode(struct DLNode *head, struct DLNode *node);
struct SLNode *addSLNode(struct SLNode *head, struct SLNode *node);
void printSLList(struct SLNode *list);
void printDLList(struct DLNode *list);
int sizeSL(struct SLNode *list);
int sizeDL(struct DLNode *list);
struct SLNode *createSecondList(struct DLNode **dblHead);
struct SLNode *createSLNodeFromDLNode(struct SLNode *head, struct DLNode *node);
void DL_del(struct DLNode *head);
void SL_del(struct SLNode *head);
void deleteNode(struct DLNode **head_ref, struct DLNode *del);

int main(){

    test();
    srand(time(0));
    struct SLNode *slHead;
    struct DLNode *head = NULL;
    int N = 10;

    for (int i = 0; i < N; i++)
    {
        struct DLNode *node = createDLNode(rand() % (10 - 1 + 1) + 1);
        if (head == NULL)
        {
            head = node;
        }
        else
        {
            head = addDLNode(head, node);
        }
    }
    printf("Doubly Linked List(%d):\n", sizeDL(head));
    printDLList(head);

    slHead = createSecondList(&head);
    puts("\n\n>>>CreateSecondList<<<");
    printf("\nDoubleLinkedList(%d):\n", sizeDL(head));
    printDLList(head);
    printf("\nSingleLinkedList(%d):\n", sizeSL(slHead));
    printSLList(slHead);
    puts("");
    assert(sizeDL(head) + sizeSL(slHead) == N);
    DL_del(head);
    SL_del(slHead);

    return 0;
}

void test()
{
    struct SLNode *slnode;
    struct DLNode *dlnode;

    slnode = createSLNode(5);
    assert(slnode->data == 5 && slnode->next == NULL);
    free(slnode);
    dlnode = createDLNode(3);
    assert(dlnode->data == 3 && dlnode->next == NULL && dlnode->prev == NULL);
    free(dlnode);

    struct SLNode *slhead = NULL;
    slnode = createSLNode(4);
    slhead = addSLNode(slhead, slnode);
    assert(slhead->data == 4);
    assert(sizeSL(slhead) == 1);

    slnode = createSLNode(6);
    slhead = addSLNode(slhead, slnode);
    assert(slhead->data == 4 && slhead->next->data == 6);
    assert(sizeSL(slhead) == 2);

    slnode = createSLNode(5);
    slhead = addSLNode(slhead, slnode);
    assert(slhead->data == 4 && slhead->next->data == 5 && slhead->next->next->data == 6);
    assert(sizeSL(slhead) == 3);

    SL_del(slhead);

    struct DLNode *dlhead = NULL;
    struct DLNode *dlit = dlhead;
    assert(sizeDL(dlhead) == 0);
    //
    dlnode = createDLNode(7);
    dlhead = addDLNode(dlhead, dlnode);
    dlit = dlhead;
    assert(sizeDL(dlhead) == 1);
    assert(dlit->data == 7);
    //
    dlnode = createDLNode(1);
    dlhead = addDLNode(dlhead, dlnode);
    dlit = dlhead;
    assert(sizeDL(dlhead) == 2);
    assert(dlhead->data == 1);
    //
    dlnode = createDLNode(-3);
    dlhead = addDLNode(dlhead, dlnode);
    dlit = dlhead;
    assert(sizeDL(dlhead) == 3);
    assert(dlit->data == -3);
    //
    dlnode = createDLNode(15);
    dlhead = addDLNode(dlhead, dlnode);
    dlit = dlhead;
    assert(sizeDL(dlhead) == 4);
    assert(dlit->data == 15);
    //
    dlnode = createDLNode(-6);
    dlhead = addDLNode(dlhead, dlnode);
    dlit = dlhead;
    assert(sizeDL(dlhead) == 5);
    assert(dlit->data == -6);

    //-6 15 -3 1 7

    slhead = createSecondList(&dlhead);
    assert(slhead->data == -6 && slhead->next->data == 1 && slhead->next->next->data == -3);
    assert(dlhead->data == 15 && dlhead->next->data == 7);
    assert(sizeDL(dlhead) + sizeSL(slhead) == 5);
    //

    //

    SL_del(slhead);
    DL_del(dlhead);

    struct DLNode *dlhead1 = NULL;
    struct DLNode *dlit1 = dlhead;
    assert(sizeDL(dlhead1) == 0);
    //
    dlnode = createDLNode(7);
    dlhead1 = addDLNode(dlhead1, dlnode);
    dlit1 = dlhead1;
    assert(sizeDL(dlhead1) == 1);
    assert(dlit1->data == 7);
    //
    dlnode = createDLNode(6);
    dlhead1 = addDLNode(dlhead1, dlnode);
    dlit1 = dlhead1;
    assert(sizeDL(dlhead1) == 2);
    assert(dlhead1->data == 6);
    //
    dlnode = createDLNode(5);
    dlhead1 = addDLNode(dlhead1, dlnode);
    dlit1 = dlhead1;
    assert(sizeDL(dlhead1) == 3);
    assert(dlit1->data == 5);

    slhead = createSecondList(&dlhead1);
    assert(slhead == NULL);
    assert(dlhead1->data == 5 && dlhead1->next->data == 6 && dlhead1->next->next->data == 7);
    assert(sizeDL(dlhead1) + sizeSL(slhead) == 3);
    SL_del(slhead);
    DL_del(dlhead1);


    dlhead = NULL;
    dlit = dlhead;
    assert(sizeDL(dlhead) == 0);
    dlnode = createDLNode(4);
    dlhead = addDLNode(dlhead, dlnode);
    dlit = dlhead;
    assert(sizeDL(dlhead) == 1);
    assert(dlit->data == 4);
    //
    dlnode = createDLNode(1);
    dlhead = addDLNode(dlhead, dlnode);
    dlit = dlhead;
    assert(sizeDL(dlhead) == 2);
    assert(dlhead->data == 1);
    //
    dlnode = createDLNode(-3);
    dlhead = addDLNode(dlhead, dlnode);
    dlit = dlhead;
    assert(sizeDL(dlhead) == 3);
    assert(dlit->data == -3);

    slhead = createSecondList(&dlhead);
    assert(slhead->data == -3 && slhead->next->data == 4 && slhead->next->next->data == 1);
    assert(dlhead == NULL);
    assert(sizeDL(dlhead) + sizeSL(slhead) == 3);
    SL_del(slhead);
    DL_del(dlhead);
}

struct SLNode *createSLNode(int data)
{
    struct SLNode *list;
    list = (struct SLNode *)malloc(sizeof(struct SLNode));
    list->data = data;
    list->next = NULL;
    return list;
}
struct SLNode *addSLNode(struct SLNode *head, struct SLNode *node)
{
    struct SLNode *minimal;
    struct SLNode *elem;
    elem = head;
    minimal = head;
    if (head != NULL)
    {
        while (elem != NULL)
        {
            if (elem->data < minimal->data)
            {
                minimal = elem;
            }
            elem = elem->next;
        }

        node->next = minimal->next;
        minimal->next = node;
    }
    else
    {
        head = node;
    }
    return head;
}
struct DLNode *createDLNode(int data)
{
    struct DLNode *list;
    list = (struct DLNode *)malloc(sizeof(struct DLNode));
    list->data = data;
    list->next = NULL;
    list->prev = NULL;
    return list;
}

struct DLNode *addDLNode(struct DLNode *head, struct DLNode *node)
{
    if (head == NULL)
    {
        head = node;
    }
    else
    {
        node->prev = head->prev;
        head->prev = node;
        node->next = head;
    }
    return node;
}

void printSLList(struct SLNode *list)
{
    struct SLNode *iterator = list;
    while (iterator != NULL)
    {
        printf("%i ", iterator->data);
        iterator = iterator->next;
    }
}

//6
void printDLList(struct DLNode *list)
{
    struct DLNode *iterator = list;
    while (iterator != NULL)
    {
        printf("%i ", iterator->data);
        iterator = iterator->next;
    }
}

int sizeSL(struct SLNode *list)
{
    int counter = 0;
    struct SLNode *iter = list;
    while (iter != NULL)
    {
        counter++;
        iter = iter->next;
    }

    return counter;
}

int sizeDL(struct DLNode *list)
{
    int counter = 0;
    struct DLNode *iter = list;
    while (iter != NULL)
    {
        counter++;
        iter = iter->next;
    }

    return counter;
}

struct SLNode *createSecondList(struct DLNode **dblHead)
{
    struct SLNode *head = NULL;
    struct DLNode *dlHead = *dblHead;
    struct DLNode *node = dlHead;
    struct DLNode *node1 = NULL;

    while (node != NULL)
    {
        node1 = node->next;
        if (node->data < 5)
        {
            if (node->prev != NULL)
            {
                node->prev->next = node->next;
            }
            else
            {
                dlHead = node->next;
            }
            if (node->next != NULL)
            {
                node->next->prev = node->prev;
            }
            head = createSLNodeFromDLNode(head, node);
        }
        node = node1;
    }
    *dblHead = dlHead;
    return head;
}

struct SLNode *createSLNodeFromDLNode(struct SLNode *head, struct DLNode *node)
{

    int buffi = node->data;
    free(node);
    struct SLNode *newSL = createSLNode(buffi);
    struct SLNode *newhead = addSLNode(head, newSL);
    return newhead;
}

void DL_del(struct DLNode *head)
{
    struct DLNode *iterator = head;
    struct DLNode *tmp = NULL;

    while (iterator != NULL)
    {
        tmp = iterator;
        iterator = iterator->next;
        free(tmp);
    }
    head = NULL;
}
void SL_del(struct SLNode *head)
{
    struct SLNode *iterator = head;
    struct SLNode *tmp = NULL;

    while (iterator != NULL)
    {
        tmp = iterator;
        iterator = iterator->next;
        free(tmp);
    }
    head = NULL;
}
