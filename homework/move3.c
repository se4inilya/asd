#include <stdio.h>
#include <stdlib.h>
#define SIZE 5

int main()
{
    int arr[SIZE] = {1, 2, 3, 4, 5};
    int k = arr[SIZE - 1];
    for(int i = SIZE - 2; i >= 0; i--){
        arr[i + 1] = arr[i];
    }
    arr[0] = k;

    for (int i = 0; i < SIZE; i++)
    {
        printf("%i  ", arr[i]);
    }

    return 0;
}