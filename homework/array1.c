#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define SIZE 10

void swap(int *num1, int *num2)
{
    int buf = *num1;
    *num1 = *num2;
    *num2 = buf;
    *num2 = buf;
}

int main()
{
    int arr[SIZE];
    srand(time(0));
    int max = 11;
    int min = -11;
    int *fNeg;
    int *lPos;
    bool firstNeg = false;

    for (int i = 0; i < SIZE; i++)
    {
        arr[i] = rand() % (max - min + 1) + min;
        printf("%i  ", arr[i]);
    }
    printf("\n");

    for (int i = 0; i < SIZE; i++)
    {
        if (firstNeg == false && arr[i] < 0)
        {
            fNeg = &arr[i];
            firstNeg = true;
        }

        if(arr[i] > 0)
        {
            lPos = &arr[i];
        }

        if(i == SIZE - 1)
        {
            printf("\n%i\n%i\n\n", *fNeg, *lPos);
            swap(fNeg, lPos);

            for(int j = 0; j < SIZE; j++)
            {
                printf("%i  ", arr[j]);
            }
            printf("\n");
        }
    }

    return 0;
}